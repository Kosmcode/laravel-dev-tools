<?php

return [
    'test' => [
        'unit' => [
            'testPath' => base_path('tests/Unit'),
            'testNamespace' => 'Tests\Unit',
            'templateName' => 'vendor.kosmcode.test.unit.template',
        ]
    ]
];
