<?php

namespace Kosmcode\LDTUnitTestMaker\Tests\Files\UnitTest;

use Exception;
use Illuminate\Config\Repository;
use Illuminate\Filesystem\Filesystem as File;

class TestService implements TestServiceInterface
{
    public function __construct(
        protected readonly File       $file,
        protected readonly Repository $repository
    )
    {

    }

    public function setSomething(?string $varValue): bool
    {
        $this->doingSometimes();

        return true;
    }

    public function otherFunction(): ?string
    {
        return $this->doingSomeDiff();
    }

    public function returnSelfClass(): self
    {
        return $this;
    }

    public function returnClass(): Exception
    {
        return new Exception();
    }

    private function doingSometimes(): void
    {
        sleep(0);
    }

    protected function doingSomeDiff(): ?string
    {
        return null;
    }

}
