<?php

namespace Kosmcode\LDTUnitTestMaker\Tests\Files\UnitTest;

use Exception;

interface TestServiceInterface
{
    public function setSomething(?string $varValue): bool;

    public function otherFunction(): ?string;

    public function returnSelfClass(): self;

    public function returnClass(): Exception;
}
