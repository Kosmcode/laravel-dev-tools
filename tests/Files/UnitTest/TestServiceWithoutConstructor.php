<?php

namespace Kosmcode\LaravelDevTools\Tests\Files\UnitTest;

class TestServiceWithoutConstructor
{
    public function testMethod(iterable $testIterable): void
    {
        return;
    }

    public static function testStaticMethod(bool $testBoolean = false): bool
    {
        return true;
    }
}
