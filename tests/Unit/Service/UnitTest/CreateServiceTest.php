<?php

use Illuminate\Config\Repository as Config;
use Illuminate\Filesystem\Filesystem as File;
use Illuminate\View\Factory as View;
use Kosmcode\LDTUnitTestMaker\Service\UnitTest\CreateService;
use Kosmcode\LDTUnitTestMaker\Tests\Files\UnitTest\TestService;
use Kosmcode\LDTUnitTestMaker\Tests\TestCase;
use Mockery\MockInterface;

class CreateServiceTest extends TestCase
{
    private $viewFactoryMock;
    private $config;
    private $file;

    public function testCreateFromReflectionClassWhenTestFileExists(): void
    {
        $this->config = $this->mock(Config::class, function (MockInterface $mock) {
            $mock->shouldReceive('get')
                ->withAnyArgs()
                ->andReturn('config');
        });
        $this->viewFactoryMock = $this->mock(View::class, function (MockInterface $mock) {
        });
        $this->file = $this->mock(File::class, function (MockInterface $mock) {
            $mock->shouldReceive('exists')
                ->once()
                ->withAnyArgs()
                ->andReturn(true);
        });

        $createService = $this->getCreateServiceMock();

        $this->assertFalse($createService->createFromReflectionClass(new ReflectionClass(TestService::class)));
        $this->assertIsString($createService->getErrorMessage());
    }

    public function testCreateFromReflectionClassWhenViewNotExists(): void
    {
        $this->config = $this->mock(Config::class, function (MockInterface $mock) {
            $mock->shouldReceive('get')
                ->withAnyArgs()
                ->andReturn('config');
        });
        $this->viewFactoryMock = $this->mock(View::class, function (MockInterface $mock) {
            $mock->shouldReceive('exists')
                ->once()
                ->withAnyArgs()
                ->andReturn(false);
        });
        $this->file = $this->mock(File::class, function (MockInterface $mock) {
            $mock->shouldReceive('exists')
                ->once()
                ->withAnyArgs()
                ->andReturn(false);
        });

        $createService = $this->getCreateServiceMock();

        $this->assertFalse($createService->createFromReflectionClass(new ReflectionClass(TestService::class)));
        $this->assertIsString($createService->getErrorMessage());
    }

    public function testCreateFromReflectionClassWhenDirectoryNotExistsAndCantCreate(): void
    {
        $this->config = $this->mock(Config::class, function (MockInterface $mock) {
            $mock->shouldReceive('get')
                ->withAnyArgs()
                ->andReturn('config');
        });
        $this->viewFactoryMock = $this->mock(View::class, function (MockInterface $mock) {
            $mock->shouldReceive('exists')
                ->once()
                ->withAnyArgs()
                ->andReturn(true);
            $mock->shouldReceive('make')
                ->once()
                ->withAnyArgs()
                ->andReturnSelf();
            $mock->shouldReceive('render')
                ->once()
                ->withAnyArgs()
                ->andReturn('renderedTest');
        });
        $this->file = $this->mock(File::class, function (MockInterface $mock) {
            $mock->shouldReceive('exists')
                ->once()
                ->withAnyArgs()
                ->andReturn(false);
            $mock->shouldReceive('isDirectory')
                ->once()
                ->withAnyArgs()
                ->andReturn(false);
            $mock->shouldReceive('makeDirectory')
                ->once()
                ->withAnyArgs()
                ->andReturn(false);
        });

        $createService = $this->getCreateServiceMock();

        $this->assertFalse($createService->createFromReflectionClass(new ReflectionClass(TestService::class)));
        $this->assertIsString($createService->getErrorMessage());
    }

    public function testCreateFromReflectionClassWhenDirectoryExistsAndCantSaveFile(): void
    {
        $this->config = $this->mock(Config::class, function (MockInterface $mock) {
            $mock->shouldReceive('get')
                ->withAnyArgs()
                ->andReturn('config');
        });
        $this->viewFactoryMock = $this->mock(View::class, function (MockInterface $mock) {
            $mock->shouldReceive('exists')
                ->once()
                ->withAnyArgs()
                ->andReturn(true);
            $mock->shouldReceive('make')
                ->once()
                ->withAnyArgs()
                ->andReturnSelf();
            $mock->shouldReceive('render')
                ->once()
                ->withAnyArgs()
                ->andReturn('renderedTest');
        });
        $this->file = $this->mock(File::class, function (MockInterface $mock) {
            $mock->shouldReceive('exists')
                ->once()
                ->withAnyArgs()
                ->andReturn(false);
            $mock->shouldReceive('isDirectory')
                ->once()
                ->withAnyArgs()
                ->andReturn(true);
            $mock->shouldReceive('put')
                ->once()
                ->withAnyArgs()
                ->andReturn(false);
        });

        $createService = $this->getCreateServiceMock();

        $this->assertFalse($createService->createFromReflectionClass(new ReflectionClass(TestService::class)));
        $this->assertIsString($createService->getErrorMessage());
    }

    public function testCreateFromReflectionClassWhenDirectoryExistsAndCanSaveFile(): void
    {
        $this->config = $this->mock(Config::class, function (MockInterface $mock) {
            $mock->shouldReceive('get')
                ->withAnyArgs()
                ->andReturn('config');
        });
        $this->viewFactoryMock = $this->mock(View::class, function (MockInterface $mock) {
            $mock->shouldReceive('exists')
                ->once()
                ->withAnyArgs()
                ->andReturn(true);
            $mock->shouldReceive('make')
                ->once()
                ->withAnyArgs()
                ->andReturnSelf();
            $mock->shouldReceive('render')
                ->once()
                ->withAnyArgs()
                ->andReturn('renderedTest');
        });
        $this->file = $this->mock(File::class, function (MockInterface $mock) {
            $mock->shouldReceive('exists')
                ->once()
                ->withAnyArgs()
                ->andReturn(false);
            $mock->shouldReceive('isDirectory')
                ->once()
                ->withAnyArgs()
                ->andReturn(true);
            $mock->shouldReceive('put')
                ->once()
                ->withAnyArgs()
                ->andReturn(true);
        });

        $createService = $this->getCreateServiceMock();

        $this->assertTrue($createService->createFromReflectionClass(new ReflectionClass(TestService::class)));
        $this->assertNull($createService->getErrorMessage());
        $this->assertIsString($createService->getTestFilepath());
    }

    protected function getCreateServiceMock(): CreateService
    {
        return new CreateService(
            $this->viewFactoryMock,
            $this->config,
            $this->file
        );
    }
}
