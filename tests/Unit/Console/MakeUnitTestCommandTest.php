<?php

namespace Kosmcode\LaravelDevTools\Tests\Unit\Console;

use Kosmcode\LDTUnitTestMaker\Service\UnitTest\CreateServiceInterface;
use Kosmcode\LDTUnitTestMaker\Tests\TestCase;
use Mockery;

class MakeUnitTestCommandTest extends TestCase
{
    public function testHandleWhenNotExistsClassNamespace(): void
    {
        $this->app->instance(
            CreateServiceInterface::class,
            Mockery::mock(
                CreateServiceInterface::class,
                function ($mock) {
                }
            )
        );

        $this->artisan("make:unit-test-template Not\\\Exists\\\Class")
            ->assertFailed();
    }

    public function testHandleWhenFailGenerateUnitTest(): void
    {
        $this->app->instance(
            CreateServiceInterface::class,
            Mockery::mock(
                CreateServiceInterface::class,
                function ($mock) {
                    $mock
                        ->shouldReceive('createFromReflectionClass')
                        ->once()
                        ->andReturn(false);
                    $mock
                        ->shouldReceive('getErrorMessage')
                        ->once()
                        ->andReturn('Error message');
                }
            )
        );

        $this->artisan("make:unit-test-template Kosmcode\\\LDTUnitTestMaker\\\Tests\\\Files\\\UnitTest\\\TestService")
            ->assertFailed();
    }

    public function testHandleSuccess(): void
    {
        $this->app->instance(
            CreateServiceInterface::class,
            Mockery::mock(
                CreateServiceInterface::class,
                function ($mock) {
                    $mock
                        ->shouldReceive('createFromReflectionClass')
                        ->once()
                        ->andReturn(true);
                    $mock
                        ->shouldReceive('getTestFilepath')
                        ->once()
                        ->andReturn('file/path/to/generated/unit/test');
                }
            )
        );

        $this->artisan("make:unit-test-template Kosmcode\\\LDTUnitTestMaker\\\Console\\\MakeUnitTestCommand")
            ->assertSuccessful();
    }
}
