<?php

namespace Kosmcode\LDTUnitTestMaker\Tests;

use Kosmcode\LDTUnitTestMaker\UnitTestMakerServiceProvider;

class TestCase extends \Orchestra\Testbench\TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    protected function getPackageProviders($app): array
    {
        return [
            UnitTestMakerServiceProvider::class,
        ];
    }

    protected function getEnvironmentSetUp($app)
    {

    }
}
