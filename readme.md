# Laravel Dev Tool - Unit Test Maker

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![Latest Version on Packagist][ico-version]][link-packagist]

Package to Laravel framework to make Unit test from exists classes

## Installation

Via Composer

``` bash
$ composer require kosmcode/ldt-unit-test-maker --dev
```

Publish service provider

``` bash
$ php artisan vendor:publish --provider="Kosmcode\LDTUnitTestMaker\UnitTestMakerServiceProvider"
```

In `resources/views/vendor/kosmcode/test/unit/template.blade.php` you can change template for create UnitTest class (add
phpDocs with copyright etc.)

## Usage

### Unit test maker

Command

``` bash
php artisan make:unit-test-template {classNamespace}`

eg.: php artisan make:unit-test-template App\\Service\\TestService
```

## Change log

Please see the [changelog](changelog.md) for more information on what has changed recently.

## Testing

``` bash
$ composer test
```

## Credits

- [KosmCODE Maksym Kawelski][link-author]

## License

MIT License

Copyright (c) 2023 KosmCODE Maksym Kawelski

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

[ico-version]: https://img.shields.io/packagist/v/kosmcode/dev-tools.svg?style=flat-square

[ico-downloads]: https://img.shields.io/packagist/dt/kosmcode/dev-tools.svg?style=flat-square

[ico-travis]: https://img.shields.io/travis/kosmcode/dev-tools/master.svg?style=flat-square

[ico-styleci]: https://styleci.io/repos/12345678/shield

[link-packagist]: https://packagist.org/packages/kosmcode/ldt-unit-test-maker

[link-downloads]: https://packagist.org/packages/kosmcode/ldt-unit-test-maker

[link-author]: https://gitlab.com/kosmcode
