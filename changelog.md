# Changelog

All notable changes to `Laravel Dev Tool - Unit Test Maker` will be documented in this file.

## Version 1.0.1
- Fix test of package
- Fix issue for make test for class without constructor method
- Fix template blade
- Add support for 8.3 in composer.json

## Version 1.0.0

- Clean code
- Fix resource template
- Fix Readme
- Major version of package

## Version 0.1.2

- Fix template for unit test

## Version 0.1.1

- Fix template for unit test

## Version 0.1.0

- Init package
- Add command to create unit test template
- Add unit tests
