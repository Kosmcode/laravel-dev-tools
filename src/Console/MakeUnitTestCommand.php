<?php

namespace Kosmcode\LDTUnitTestMaker\Console;

use Illuminate\Console\Command;
use Kosmcode\LDTUnitTestMaker\Service\UnitTest\CreateServiceInterface;
use ReflectionClass;
use ReflectionException;

class MakeUnitTestCommand extends Command
{
    protected $signature = 'make:unit-test-template {classNamespace : Namespace to class}';
    protected $description = 'Command to make unit test template of class';

    public function handle(
        CreateServiceInterface $unitTestCreateService
    ): int
    {
        $classNamespace = $this->argument('classNamespace');

        try {
            $class = new ReflectionClass($classNamespace);
        } catch (ReflectionException $exception) {
            $this->error($exception->getMessage());

            return Command::FAILURE;
        }

        $result = $unitTestCreateService->createFromReflectionClass($class);

        if ($result === false) {
            $this->error($unitTestCreateService->getErrorMessage());

            return Command::FAILURE;
        }

        $this->info('UnitTest template saved in: `' . $unitTestCreateService->getTestFilepath() . '`');

        return Command::SUCCESS;
    }
}
