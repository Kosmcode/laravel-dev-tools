<?php

namespace Kosmcode\LDTUnitTestMaker;

use Illuminate\Support\ServiceProvider;
use Kosmcode\LDTUnitTestMaker\Console\MakeUnitTestCommand;
use Kosmcode\LDTUnitTestMaker\Service\UnitTest\CreateService;
use Kosmcode\LDTUnitTestMaker\Service\UnitTest\CreateServiceInterface;

class UnitTestMakerServiceProvider extends ServiceProvider
{
    public array $bindings = [
        CreateServiceInterface::class => CreateService::class,
    ];

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot(): void
    {
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'kosmcode');
        // $this->loadViewsFrom(__DIR__.'/../resources/views', 'kosmcode');
        // $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        // $this->loadRoutesFrom(__DIR__.'/routes.php');

        // Publishing the configuration file.
        $this->publishes([
            __DIR__ . '/../config/ldt-unit-test-maker.php' => config_path('ldt-unit-test-maker.php'),
        ]);

        // Publishing the views.
        $this->publishes([
            __DIR__ . '/resources/views' => base_path('resources/views/vendor/kosmcode'),
        ]);

        // Publishing is only necessary when using the CLI.
        if ($this->app->runningInConsole()) {
            $this->bootForConsole();
        }
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/ldt-unit-test-maker.php', 'ldt-unit-test-maker');

        // Register the service the package provides.
//        $this->app->singleton('dev-tools', function ($app) {
//            return new DevTools;
//        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides(): array
    {
        return ['ldt-unit-test-maker'];
    }

    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole(): void
    {
        // Publishing assets.
        /*$this->publishes([
            __DIR__.'/../resources/assets' => public_path('vendor/kosmcode'),
        ], 'dev-tools.views');*/

        // Publishing the translation files.
        /*$this->publishes([
            __DIR__.'/../resources/lang' => resource_path('lang/vendor/kosmcode'),
        ], 'dev-tools.views');*/

        // Registering package commands.
        $this->commands([MakeUnitTestCommand::class]);
    }
}
