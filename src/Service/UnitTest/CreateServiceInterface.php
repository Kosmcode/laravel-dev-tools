<?php

namespace Kosmcode\LDTUnitTestMaker\Service\UnitTest;

use ReflectionClass;

interface CreateServiceInterface
{
    const CONFIG_KEY_SUFFIX = 'ldt-unit-test-maker.test.unit.';

    public function createFromReflectionClass(
        ReflectionClass $class
    ): bool;

    public function getTestFilepath(): string;

    public function getErrorMessage(): ?string;
}
