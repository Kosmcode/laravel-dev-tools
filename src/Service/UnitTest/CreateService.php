<?php

namespace Kosmcode\LDTUnitTestMaker\Service\UnitTest;

use Illuminate\Config\Repository as Config;
use Illuminate\Filesystem\Filesystem as File;
use Illuminate\View\Factory as View;
use ReflectionClass;
use ReflectionMethod;

class CreateService implements CreateServiceInterface
{
    protected View $viewFactory;
    protected Config $config;
    protected File $file;

    protected ReflectionClass $class;
    protected array $templateData;
    protected string $testFilepath;
    protected ?string $errorMessage = null;


    public function __construct(
        View   $viewFactory,
        Config $config,
        File   $file
    )
    {
        $this->viewFactory = $viewFactory;
        $this->config = $config;
        $this->file = $file;
    }

    public function createFromReflectionClass(
        ReflectionClass $class
    ): bool
    {
        $this->class = $class;

        $this->prepareTestFilepath();

        if ($this->file->exists($this->testFilepath)) {
            $this->errorMessage = 'File of UnitTest already exists (filepath: `' . $this->testFilepath . '`)';

            return false;
        }

        $this->templateData = [
            'testNamespace' => null,
            'className' => null,
            'classNamespace' => null,
            'dependencyInjections' => null,
            'methods' => null,
        ];

        $this->prepareBasicDataClass();
        $this->prepareDependencyInjections();
        $this->preparePublicMethods();

        $renderedTest = $this->renderTestView();

        if ($renderedTest === null) {
            return false;
        }

        return $this->saveTestFile($renderedTest);
    }

    public function getTestFilepath(): string
    {
        return $this->testFilepath;
    }

    public function getErrorMessage(): ?string
    {
        return $this->errorMessage;
    }

    protected function prepareTestFilepath(): void
    {
        $testClassFilepath = str_replace(
                '\\',
                DIRECTORY_SEPARATOR,
                ltrim($this->class->getName(), 'App\\')
            ) . 'Test.php';

        $this->testFilepath = $this->config->get(self::CONFIG_KEY_SUFFIX . 'testPath')
            . DIRECTORY_SEPARATOR
            . $testClassFilepath;
    }

    protected function prepareBasicDataClass(): void
    {
        $unitTestNamespace = $this->config->get(self::CONFIG_KEY_SUFFIX . 'testNamespace');

        $this->templateData = [
            'testNamespace' => $unitTestNamespace . '\\' . ltrim($this->class->getNamespaceName(), 'App\\'),
            'className' => $this->class->getShortName(),
            'classNamespace' => $this->class->getName(),
        ];
    }

    protected function prepareDependencyInjections(): void
    {
        $constructorClass = $this->class->getConstructor();

        if (!$constructorClass) {
            $this->templateData['dependencyInjections'] = [];

            return;
        }

        foreach ($constructorClass->getParameters() as $parameter) {
            $this->templateData['dependencyInjections'][] = [
                'variableName' => $parameter->name,
                'className' => last(explode('\\', $parameter->getType()->getName())),
                'namespace' => $parameter->getType()->getName(),
            ];
        }
    }

    protected function preparePublicMethods(): void
    {
        foreach ($this->class->getMethods(ReflectionMethod::IS_PUBLIC) as $method) {
            if ($method->getName() === '__construct') {
                continue;
            }

            $this->templateData['methods'][] = [
                'name' => $method->getName(),
            ];
        }
    }

    protected function renderTestView(): ?string
    {
        if (!$this->viewFactory->exists($this->config->get(self::CONFIG_KEY_SUFFIX . 'templateName'))) {
            $this->errorMessage = 'Template for create unit test not exists (Did you remember about publish assets?)';

            return null;
        }

        $viewRendered = $this->viewFactory->make(
            $this->config->get(self::CONFIG_KEY_SUFFIX . 'templateName'),
            $this->templateData
        )->render();

        return '<?php' . PHP_EOL . PHP_EOL . $viewRendered;
    }

    protected function saveTestFile(string $testContent): bool
    {
        $testPath = explode(DIRECTORY_SEPARATOR, $this->testFilepath);
        unset($testPath[array_key_last($testPath)]);
        $testPath = implode(DIRECTORY_SEPARATOR, $testPath);

        if (!$this->file->isDirectory($testPath)) {
            $result = $this->file->makeDirectory($testPath, 0755, true);

            if ($result === false) {
                $this->errorMessage = 'Cant make directory';

                return false;
            }
        }

        $result = $this->file->put(
            $this->testFilepath,
            $testContent
        );

        if ($result === false) {
            $this->errorMessage = 'Cant save file';

            return false;
        }

        return true;
    }
}
