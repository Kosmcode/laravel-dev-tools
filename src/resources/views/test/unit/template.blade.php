

namespace {{ $testNamespace }};

use Tests\TestCase;
@if($dependencyInjections)
use PHPUnit\Framework\MockObject\MockObject;
@endif
use {{ $classNamespace }};
@foreach ($dependencyInjections as $dependencyInjection)
use {{ $dependencyInjection['namespace'] }};
@endforeach

class {{ $className }}Test extends TestCase
{
@if($dependencyInjections)
@foreach($dependencyInjections as $dependencyInjection)
    private MockObject ${{ $dependencyInjection['variableName'] }}Mock;
@endforeach

    protected function setUp(): void
    {
        parent::setUp();

@foreach($dependencyInjections as $dependencyInjection)
        $this->{{ $dependencyInjection['variableName'] }}Mock = $this->createMock({{ $dependencyInjection['className'] }}::class);
@endforeach
    }
@endif

@foreach($methods as $method)
    public function test{{ ucfirst($method['name']) }}(): void
    {
        $this->markTestIncomplete('ToDo');

        ${{ lcfirst($className) }}Mock = $this->get{{ $className }}Mock();
        $result = ${{ lcfirst($className) }}Mock->{{ $method['name'] }}();
    }

@endforeach
    private function get{{ $className }}Mock(): {{ $className }}
    {
@if($dependencyInjections)
        return new {{ $className }}(
@foreach($dependencyInjections as $dependencyInjection)
            $this->{{ $dependencyInjection['variableName'] }}Mock,
@endforeach
        );
@else
        return new {{ $className }}();
@endif
    }

}
